package com.te.interview.logical;

public class ReverseWordsInString {
   public static void main(String[] args) {
	   
	   //Reverse char by char in string
	   String s="java is programming language";
	    
//	   String rev="";
//	   for(int i=s.length()-1;i>=0;i--){
//	      rev=rev+s.charAt(i);
//
//	  }
//	  System.out.println(rev);
	   
	   
	   //Reverse word by word
	   String[] split=s.split(" ");
	   String rev="";
	   for(int i=split.length-1;i>=0;i--){
	      rev=rev+split[i]+" ";

	  }
	  System.out.println(rev);
}
}
