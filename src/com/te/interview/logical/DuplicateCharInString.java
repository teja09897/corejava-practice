package com.te.interview.logical;

public class DuplicateCharInString {
  public static void main(String[] args) {
	String s="kjhgfcvbnmkjh";
	int[] res=new int[256];
	for (int i = 0; i < s.length(); i++) {
		res[s.charAt(i)]++;
	}
	for (int i = 0; i < res.length; i++) {
		if (res[i]==1) {
			System.out.println((char)i+" "+res[i]);
		}
	}
}
}
