package com.te.interview.logical;

public class ReverseString {
	public static void main(String[] args) {
		
		//Approach -01
//		String s = "java";
//		String rev = "";
//		for (int i = 0; i < s.length(); i++) {
//			rev =s.charAt(i)+ rev ;
//
//		}
//		System.out.println(rev);
		
		
		//Approach -02
		String s = "java";
		String rev="";
		 for(int i=s.length()-1;i>=0;i--){
		    rev=rev+s.charAt(i);

		}
		System.out.println(rev);
		
		//remove particular char in string
		String replace = s.replace("a", "");
		System.out.println(replace);
	}
}
