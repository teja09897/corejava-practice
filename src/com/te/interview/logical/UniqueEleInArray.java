package com.te.interview.logical;

public class UniqueEleInArray {
   public static void main(String[] args) {
	int[] a= {1,5,6,8,5,4,6,9,-1,-1,0,0};
	for (int i = 0; i < a.length; i++) {
		int count=1;
		//int rep=-1;
		for (int j = i+1; j < a.length; j++) {
			if (a[i]==a[j]) {
				a[j]=0;
				count++;
			}
		}
		if (a[i]!=0&&count==1) {
			System.out.println(a[i]);
		}
	}
}
}
