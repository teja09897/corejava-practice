package com.te.interview.logical;

public class FabinocciSeries {
   public static void main(String[] args) {
	int a=0,b=1;
	int temp=0;
	System.out.print(a+" "+b+" ");
	for (int i = 0; i < 10; i++) {
		temp=a+b;
		a=b;
		b=temp;
		System.out.print(temp+" ");
	}
}
}
