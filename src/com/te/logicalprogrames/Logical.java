package com.te.logicalprogrames;

public class Logical {

	public static void main(String[] args) {
		 Logical.m1(null);
	}
	
	public static void m1(Object o) {
		System.out.println(o);
		System.out.println("object");
	}
	
	public static void m1(String s) {
		System.out.println(s);
		System.out.println("string ");
	}
}
