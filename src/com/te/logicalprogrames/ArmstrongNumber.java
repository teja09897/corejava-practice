package com.te.logicalprogrames;

public class ArmstrongNumber {
	public static void main(String[] args) {
		int num = 153;
		int count = 0;
		int temp = num;
		while (num != 0) {
			count++;
			num = num / 10;
		}
		num = temp;
		int sum = 0;
		int lastdigit = 0;
		while (num != 0) {
			lastdigit = num % 10;
			int product = 1;
			for (int i = 0; i <count; i++) {
				product = product * lastdigit;
			}
			sum = sum + product;
			num = num / 10;
			
		}
		if (temp == sum) {
          System.out.println(temp+" is an armstrong number");
		}
		else {
			 System.out.println(temp+" is not an armstrong number");
		}
       


	}
}
