package com.te.logicalprogrames;
//sum and product of the all the digits in a given number 
public class SpyNumber {
  public static void main(String[] args) {
	int num=123;
	int sum=0;
	int product=1;
	int lastdigit;
	while (num!=0) {
		lastdigit=num%10;
		product=product*lastdigit;
		sum=sum+lastdigit;
		num=num/10;
	}
	if (product==sum) {
		System.out.println("given number is spy number");
	}
}
}
