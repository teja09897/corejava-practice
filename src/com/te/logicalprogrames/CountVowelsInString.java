package com.te.logicalprogrames;

import java.util.Scanner;

public class CountVowelsInString {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter a String");
		String string = scanner.next();
		int vowels = 0;
		int consonants = 0;
		for (int i = 0; i < string.length(); i++) {
			char charAt = string.charAt(i);
			if (charAt >= 65 && charAt <= 90 || charAt >= 97 && charAt <= 122) {
				if (charAt == 'a' || charAt == 'e' || charAt == 'i' || charAt == 'o' || charAt == 'u') {
					vowels++;
				} else {
					consonants++;
				}
			}

		}
		System.out.println(vowels);
		System.out.println(consonants);
	}

}
