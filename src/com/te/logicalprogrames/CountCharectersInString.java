package com.te.logicalprogrames;

public class CountCharectersInString {
	public static void main(String[] args) {
		String string = "kjhg0987mnbvkvkjhgfd";
		char[] charArray = string.toCharArray();
		char[] c = new char[charArray.length];

		for (int i = 0; i < charArray.length; i++) {
			int count = 1;
			char visited = '\u0000';
			for (int j = i + 1; j < charArray.length; j++) {
				if (charArray[i] == charArray[j]) {
					c[j] = visited;
					count++;
				}
			}
			if (c[i] == visited && count > 1) {
				System.out.println(charArray[i] + " fr " + count);
			}
		}

	}
}
