package com.te.logicalprogrames;

//an addition of factorial of each digit of the number, 
//which is equal to the number itself. 
public class StrongNumber {
	public static void main(String[] args) {
		int num = 145;
		int temp = num;
		int sum = 0;
		while (num != 0) {
			int lastdigit = num % 10;
			int product = 1;
			for (int i = 1; i <= lastdigit; i++) {
				product = product * i;
			}
			sum = sum + product;
			num = num / 10;
		}
		num = temp;
		if (num == sum) {
			System.out.println(num + " strong number");
		} else {
			System.out.println("not a strong number");
		}
	}
}
