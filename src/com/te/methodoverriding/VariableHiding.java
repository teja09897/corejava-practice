package com.te.methodoverriding;

class A {
	static int value = 23;

	public void m() {
		int value = 56;
		// System.out.println(this.value);
		System.out.println(value);
	}
}
public class VariableHiding extends A{
	static int value = 67;

	public static void main(String[] args) {
		System.out.println(value);
		System.out.println(System.getProperty("userName"));
	}
}
