package com.te.methodoverriding;

public class Product {
	private Integer pId;
	private String pName;
	private Double pPrice;

	public Product() {
		super();
	}

	public Product(Integer pId, String pName, Double pPrice) {
		this.pId = pId;
		this.pName = pName;
		this.pPrice = pPrice;
	}

	public void setPid(Integer pId) {
		this.pId = pId;
	}

	public Integer getPid() {
		return this.pId;
	}

	public void setPname(String pName) {
		this.pName = pName;
	}

	public String getPname() {
		return this.pName;
	}

	public void setPprice(Double pPrice) {
		this.pPrice = pPrice;
	}

	public Double getPprice() {
		return this.pPrice;
	}

	
	@Override
	public String toString() {
		return "Product [pId=" + pId+ ", pName=" +pName+", pPrice="+pPrice+ "]";
	}
	
	
}
