package com.te.methodoverriding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestProduct {
	public static void main(String[] args) {
		List<Product> products = new ArrayList<Product>();
		products.add(new Product(10, "Hp", 23.000));
		products.add(new Product(2, "Dell", 45.000));
		products.add(new Product(3, "lenovo", 56.000));
		products.add(new Product(8, "sony", 56.000));
		products.add(new Product(3, "Apple", 56.000));

		long count = products.stream().distinct().count();
		System.out.println(count);

		List<Integer> collect = List.of(1, 2, 3, 4).stream().map(a -> a * a).collect(Collectors.toList());
		System.out.println(collect);

		// flatmap():is used to merge 2 or more lists to one list
		List<List<Integer>> of = List.of(List.of(1, 2), List.of(3, 4), List.of(5, 6));
		List<Integer> collect2 = of.stream().flatMap(a -> a.stream()).collect(Collectors.toList());
		System.out.println(collect2);

		// limit(n) method is intermediate operation that returns a stream not longer
		// than the requested size
		/*
		 * filter() : It is used to filter the stream by using Predicate Function and
		 * pass them to next operation based on the result given by test() of Predicate
		 */

		/*
		 * skip(): It used to skip the elements passing to the next operation it we use
		 * 2 as a parameter then it skips first 2 elements of stream and pass other
		 * elements to next operation
		 */
		collect.stream().skip(2).forEach(e -> System.out.println(e));

		/*
		 * peek(): it will take the stream of elements and pass it to the next
		 * operation, it will take consumer function that applies on it but doesn't
		 * modify the values the purpose of this method is to support debugging so that
		 * we can the understand the flow of the elements in stream
		 */
		Stream<Integer> peek = collect.stream().peek(e -> System.out.println(e));
		peek.collect(Collectors.toList()).forEach(e -> System.err.println(e));

		// Distinct() : it is used to pass the unique elements to the next operation

		/*
		 * sorted() : it sorts the elements in the natural order and pass them to next
		 * operation
		 */

		/*
		 * sorted(comparator) : it sorts the elements in the user defined order by
		 * taking using comparator and pass them to next operation
		 */

		/*
		 * allMatch() : it is used to or check the elements in our list based on
		 * condition it will check the whether all elements satisfying the given
		 * condition if satisfied return true else false
		 */

		boolean allMatch = products.stream().allMatch(e -> e.getPprice() > 20.000);
		System.out.println(allMatch);

		boolean anyMatch = products.stream().anyMatch(e -> e.getPprice() < 35.000);
		System.out.println(anyMatch);

		products.stream().filter(e -> e.getPname().length() > 3).peek(e -> System.err.println(e))
				.map(e -> e.getPname().toUpperCase()).peek(e -> System.out.println(e))
				.forEach(e -> System.err.println(e));

		// max():it returns the maximum element in the stream
		// min():it returns the minimum element in the stream
		Optional<Product> max = products.stream().max((e1, e2) -> e1.getPprice().compareTo(e2.getPprice()));
		System.out.println(max.get());

		/*
		 * toArray() : it is used to convert the stream of elements into array
		 */

		Object[] array = products.stream().toArray();
		System.out.println(Arrays.toString(array));
		
		Optional<Integer> reduce = List.of(1,2,3,4,5,6,7).stream().reduce((a,b)->a+b);
		System.out.println(reduce.get());
		
		Integer reduceWithIdentityAccumulator = List.of(1, 2, 3, 4).stream().reduce(10, (a, b) -> a + b);
		System.out.println("reduce using identiy and accumulator :" + reduceWithIdentityAccumulator);

		Integer reduceWithIdentityAccumulatorCombiner = List.of(1, 2, 3, 4).parallelStream().reduce(0, (a, b) -> a + b,
				(a, b) -> a + b);
		System.out.println("reduce using Identity Accumulator Combiner :" + reduceWithIdentityAccumulatorCombiner);




	}
}
