package com.te.collections;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class LearnTreeMap {
   public static void main(String[] args) {
	   Employee e1=new Employee(1, "A", 23);
	   Employee e2=new Employee(9, "N", 21);
	   Employee e3=new Employee(3, "I", 20);
	   Employee e4=new Employee(4, "M", 26);

	   
	HashMap<Integer, Employee> treeMap=new HashMap<Integer, Employee>();
	treeMap.put(e1.getEmpId(), e1);
	treeMap.put(e2.getEmpId(), e2);
	treeMap.put(e3.getEmpId(), e3);
	treeMap.put(e4.getEmpId(), e4);
		   
	treeMap.entrySet().stream().sorted((o1,o2)->o1.getValue().getEmpAge()-o2.getValue().getEmpAge()).forEach(c->System.out.println(c));
	treeMap.entrySet().stream().sorted((o1,o2)->o1.getValue().getEmpId()-o2.getValue().getEmpId()).forEach(c->System.out.println(c));
	
	
	
}
}
