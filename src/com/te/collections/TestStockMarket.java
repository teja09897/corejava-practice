package com.te.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class TestStockMarket {
	public static void main(String[] args) {
	  StockMarket s1=new StockMarket(101,"afgh",1234);
	  StockMarket s2=new StockMarket(102,"uytr",1670);
	  StockMarket s3=new StockMarket(103,"iuyk",1789);
	  StockMarket s4=new StockMarket(104,"kjhg",1980);

	HashMap<Integer, StockMarket> hashMap=new HashMap<Integer, StockMarket>();
	  hashMap.put(s1.getBarcode(), s1);
	  hashMap.put(s2.getBarcode(), s2);
	  hashMap.put(s3.getBarcode(), s3);
	  hashMap.put(s4.getBarcode(), s4);

//	  hashMap.entrySet().stream().forEach(e->System.out.println(e.getValue()));
//	  hashMap.entrySet().stream().sorted((e1,e2)->e1.getValue().getPrice()-e2.getValue().getPrice()).forEach(e->System.out.println(e));
//	  
//	  System.out.println(hashMap.get(101));

		Scanner scanner = new Scanner(System.in);
		System.out.println("enter barcode");
		int barcode = scanner.nextInt();
		System.out.println("enter name of the product");
		String name = scanner.next();
		System.out.println("enter price of the product");
		int price = scanner.nextInt();

		StockMarket stockMarket = new StockMarket();
		stockMarket.setBarcode(barcode);
		stockMarket.setName(name);
		stockMarket.setPrice(price);

//		HashMap<Integer, StockMarket> hashMap = new HashMap<Integer, StockMarket>();
//		hashMap.put(barcode, stockMarket);
//
//		hashMap.entrySet().stream().filter(e -> e.getKey().equals(barcode)).forEach(e -> System.out.println(e));
//
//		System.out.println(hashMap.get(101));

	}
}
