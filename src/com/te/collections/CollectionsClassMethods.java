package com.te.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class CollectionsClassMethods {
	public static void main(String[] args) {
		List<String> list = Arrays.asList("jhgv", "kjhg", "ert", "oiuh", "mnb", "asd", "mnb");
//	String max = Collections.max(list);
//	System.out.println(max.toString());
//	
//	Collections.reverse(list);
		System.out.println(list.size());

//	Collections.shuffle(list);
//	System.out.println(list);

//	Collections.shuffle(list,new Random(1));
//	System.out.println(list);

		// swap the element in specified position in specified list
//	Collections.swap(list, 5, 5);
//	System.out.println(list);

		List<String> list2 = new ArrayList<String>();
		list2.add("jh");
		list2.add("jh");
		list2.add("jh");
		list2.add("jh");
		list2.add("jh");
		list2.add("mnb");
		// System.out.println(list2.size());
//	Collections.copy(list2, list);
//	System.out.println(list2);

		Collections.rotate(list2, 1);
		System.out.println(list2);

//	List<String> unmodifiableList = Collections.unmodifiableList(list2);
//	System.out.println(unmodifiableList);

		// unmodifiableList.add("re");

		List<String> synchronizedList = Collections.synchronizedList(list);
		System.out.println(synchronizedList);

		System.out.println(Collections.frequency(list, "mnb"));

		System.out.println(Collections.disjoint(list, list2));

	}
}
