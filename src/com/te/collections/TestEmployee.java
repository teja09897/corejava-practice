package com.te.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TestEmployee {
	public static void main(String[] args) {
	     List<Employee> employees=new ArrayList<Employee>();
         employees.add(new Employee(1, "HGFD", 23));
         employees.add(new Employee(2, "IUYT", 28));
         employees.add(new Employee(3, "MNBVC", 21));
         employees.add(new Employee(4, "WERT", 67));

         Comparator<Employee> comparator=(o1,o2)->o1.getEmpAge()-o2.getEmpAge();
         List<Employee> collect = employees.stream().sorted(comparator).collect(Collectors.toList());
        LinkedHashMap<Integer, Employee >k= collect.stream().collect(Collectors.toMap(Employee::getEmpId,Function.identity(),(k1,k2)->k1,LinkedHashMap::new));                         
                      k.entrySet().forEach(e->System.out.println(e));
           
          

	}
    
}
