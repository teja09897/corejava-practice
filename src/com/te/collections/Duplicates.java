package com.te.collections;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Duplicates {
   public static void main(String[] args) {
	List<Integer> asList = Arrays.asList(2,4,5,7,7,4);
	asList.stream().filter(e->Collections.frequency(asList, e)>0).collect(Collectors.toSet()).forEach(e->System.out.println(e));
	
	
	
}
   
}
