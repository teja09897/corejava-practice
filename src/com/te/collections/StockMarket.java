package com.te.collections;

public class StockMarket {
	private Integer barcode;
	private String name;
	private Integer price;
	public StockMarket() {
		super();
	}
	public StockMarket(Integer barcode, String name, Integer price) {
		super();
		this.barcode = barcode;
		this.name = name;
		this.price = price;
	}
	public Integer getBarcode() {
		return barcode;
	}
	public void setBarcode(Integer barcode) {
		this.barcode = barcode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "StockMarket [barcode=" + barcode + ", name=" + name + ", price=" + price + "]";
	}
	
	
}
