package com.te.practice;

class A {
	private int i = 90;
	int j = 80;

	A() {
		System.out.println("A class constructor ");
	}

	public static void s() {
		System.out.println("super");
	}

}

public class Test1 extends A {
	int j = 809;

	public Test1() {
		// super();
	}

	
	public static void s() {
		System.out.println("child");
	}

	public static void main(String[] args) {
		A a = new Test1();
		System.out.println(a.j);
		a.s();
	}
}
