package com.te.practice;

class Aa {
	// constructors,initializers and private members we canot inherit
	// main( ) we can inherit
//	No.
//
//	Static blocks are executed when a class is loaded and are concerned with initializing
//	only static members (which are not related with the object of class) . So they dont participate in inheritance.
//	Instance initialization blocks become part of the constructor and execute after the super() statement at object creation. As constructors cannot be inherited and can be accessed only through super() which is already the first staement in any constructor it dosent make sense to make it participate in inheritance(the creators of java were smart enough :) )

	public static int a = 10;
	
	public int j=20;

	private void name() {
		System.out.println("private method");
	}

	Aa() {
		System.out.println("A constructor");
	}

	{
		System.out.println("non static multiline initializer");
	}

	public static void main(String[] args) {
		System.out.println("main is inherited");
	}
}

public class Demo extends Aa{
	//public static int a = 20;
	public static void main(String[] args) {
		System.out.println("main is inherited 2");
		Aa.main(args);
		System.out.println(Aa.a);
		//Aa aa=new Aa();
		Demo demo=new Demo();
		System.out.println(demo.j);
	}
}
