package com.te.inheritence;
class A{
	int i=10;
}
class B extends A{
	int i=20;
}
public class LMI {
    public static void main(String[] args) {
    	A a=new A();
    	B b=new B();
    	System.out.println(b.i);
	}
}
