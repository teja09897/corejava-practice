package com.te.method.hiding;
class A{
	 public static void m1() {
		System.out.println("parent");
	}
	 public void m2() {
		System.out.println("parent non-static");
	}
}
class B extends A{
	
	public static void m1() {
		System.out.println("child");
	}
	@Override
	public void m2() {
		System.out.println("child non-static");
	}
	
}
public class MethodHiding {
  public static void main(String[] args) {
	  A b=new B();
	   b.m1();
	   b.m2();
}
}
