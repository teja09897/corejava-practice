package com.te.learn.multithreding;

import java.util.Iterator;

class Thread01 extends Thread{
	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			try {Thread.sleep(1000);}catch(Exception e) {}
			System.out.println(Thread.currentThread().getName());
		}
	}
}

public class DeamonThreads {
  public static void main(String[] args) {
	System.out.println(Thread.currentThread().isDaemon());
	//Thread.currentThread().setDaemon(true);
	Thread01 t1=new Thread01();
	t1.setDaemon(true);
	t1.start();
	//t1.setDaemon(true);
	System.out.println(t1.isDaemon());
	System.out.println("end of main thread");
}
}
