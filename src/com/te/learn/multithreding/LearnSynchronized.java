package com.te.learn.multithreding;
class Counter{
	int count=0;
	public synchronized void increament() {
		count++;
	}
}
public class LearnSynchronized {
     public static void main(String[] args) throws Exception{
    	 Counter c=new Counter();
         c.increament();
//         System.out.println(c.count);
         
         Thread t1=new Thread(new Runnable()  {
				public void run() {
				for (int i = 0; i <1000; i++) {
					c.increament();
				}
			}
         }
         );
         Thread t2=new Thread(new Runnable()  {
				public void run() {
				for (int i = 0; i <1000; i++) {
					c.increament();
				}
			}
      }
      );
      
         t1.start();
         t2.start();
         
         t1.join();
         t2.join();
         System.out.println(c.count);
	}
}
