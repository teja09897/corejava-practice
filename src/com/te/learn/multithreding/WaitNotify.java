package com.te.learn.multithreding;
class MyThreadt extends Thread{
	
	@Override
	public synchronized void run() {
		for (int i = 0; i < 5; i++) {
			System.out.println("child");
		}
		this.notify();
	}
	
	
}
public class WaitNotify {
public static void main(String[] args) throws InterruptedException{
	MyThreadt t=new MyThreadt();
	t.start();
	Thread.sleep(10000);
	//t.sleep(5000);
	//t.join(10000);
	synchronized (t) {
		t.wait(2000);
	}
	for (int i = 0; i < 5; i++) {
		System.out.println("main");
	}
}
}
