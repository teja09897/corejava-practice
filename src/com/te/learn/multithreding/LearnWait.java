package com.te.learn.multithreding;

class Wait extends Thread {
	int total = 0;

	@Override
	public void run() {
		synchronized (this) {
			for (int i = 0; i < 10; i++) {
				total = total + 100;
			//	System.out.println(total);
			}
			//this.notify();
		}
	}
}
public class LearnWait {
	public static void main(String[] args) throws InterruptedException {
		Wait wait = new Wait();
		wait.start();
		// System.out.println(wait.total);
		synchronized (wait) {
			wait.wait();
		}

		System.out.println(wait.total);
	}

}
