package com.te.learn.multithreding;

class MyThreads implements Runnable{ 
	int total=0;
	@Override
	public void run() {
		for (int i = 0; i < 5; i++) {
			total=total+1;
			System.out.println(Thread.currentThread().getName()+total);
		}
	}
	
	
	
	
}
public class LearnSleepMethod {
	
  public static void main(String[] args) throws InterruptedException {
	System.out.println("before");
	  Thread.sleep(2000);
	System.out.println("after");
	System.out.println(Thread.currentThread().getId());
	System.out.println(Thread.currentThread().getName());
	System.out.println(Thread.currentThread().getPriority());
	System.out.println(Thread.currentThread().isAlive());
	System.out.println(Thread.currentThread().isDaemon());
	System.out.println(Thread.MIN_PRIORITY);
	System.out.println(Thread.NORM_PRIORITY);
	System.out.println(Thread.activeCount());
	System.out.println(Thread.interrupted());
	
	MyThreads m=new MyThreads();
	Thread t1=new Thread(m);
	Thread t2=new Thread(t1);
	
	t1.start();
	t2.start();
  }
	
	

}
