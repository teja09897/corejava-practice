package com.te.learn.multithreding;

class MyThread extends Thread{
	int sum=0;
	public synchronized void run(){
		System.out.println("child thread starts calculation");
		for (int i = 0; i < 5; i++) {
			sum=sum+1;
			System.out.println("child"+sum);
		}
		System.out.println("child thread giving notification");

	//	this.notify();
		
	}
	
}
public class SleepMethod {
     public synchronized static void main(String[] args) throws InterruptedException {
    	 MyThread t=new MyThread();
		t.start();
		t.sleep(10000);//if child thread got chance to execute first,no thread give notification we should use wait with timeperiod.
		//t.join();//this method will executed by main thread .main threead entered into waiting state untill child thread execute its execution
		System.out.println("main thread calling wait method");
//		synchronized (t) {
//			t.wait(2000);
//		}
	//	t.sleep(6000);
		System.out.println("main thread got notification");
		System.out.println(t.sum);



		
		
		
			
		
	}
}
