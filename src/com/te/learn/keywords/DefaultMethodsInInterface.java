package com.te.learn.keywords;

import java.util.Collection;

interface Demo1 {
	default void m() {
      System.out.println("m in 1");
	}
}

interface Demo2 {
	default void m() {
	      System.out.println("m in 2");

	}
}

class DemoImp implements Demo1, Demo2 {

	@Override
	public void m() {
		// TODO Auto-generated method stub
		Demo2.super.m();
	}
}

public class DefaultMethodsInInterface {
     DemoImpl demoImpl=new DemoImpl();
}
