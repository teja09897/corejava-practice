package com.te.learn.keywords;

public class StaticKeyword {
	static int i=10;
	static {
		System.out.println("static block");
	}
	
	public static void name() {
		System.out.println("static method");
		//System.out.println(i);
	}
	
	public static void main(String[] args) {
		
		System.out.println(i);
		name();
	}

}
