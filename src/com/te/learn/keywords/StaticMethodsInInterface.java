package com.te.learn.keywords;
interface Demo{
	static int j=30;
	int i=10;
	void show();
	public static void m() {
		System.out.println("static method is called "+j);
	}
	
}
class DemoImpl implements Demo{
	
	@Override
	public void show() {
		
		Demo.m();
		
		
	}
	
}
public class StaticMethodsInInterface {
public static void main(String[] args) {
	DemoImpl demoImpl=new DemoImpl();
	demoImpl.show();
	System.out.println(Demo.j);
}
}
