package com.te.learn.keywords;

public class LearnKeyWord {
	String name;
	int age;
    static int i=90;
    
    public LearnKeyWord (int age,String name) {
		this.age=age;
		this.name=name;
		this.i=i;
	}
    
    public void run() {
		System.out.println(age+" "+name+" "+i);
	}
    
    
    public static void main(String[] args) {  
    	LearnKeyWord kw=new LearnKeyWord(23,"fds");
//    	kw.name="kjh";
//    	kw.age=8;
    	kw.run();
    	
    	LearnKeyWord kw2=new LearnKeyWord(34,"gfd");
//    	kw2.name="kjh";
//    	kw2.age=9;
    	kw2.run();
    	
    	
	}
}
