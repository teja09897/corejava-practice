package com.te.learn.keywords;
public class StaticKw {
	int i = 10;

	public static void m() {
		int i = 20;
		//System.out.println(this.i);
		System.out.println("static method");
		//m2();

	}

	public void m2() {
		m();
		System.out.println(this.i);
		System.out.println("m2 nonstatic ");
	}

	public static void main(String[] args) {
		m();
		StaticKw static1=new StaticKw();
		static1.m2();
	}
}
