package com.te.learn.keywords;

public class Static1{
	  int i=10;
	   static int l;
	static int j=90;
	
	public static void name() {
	
		System.out.println(j);
		// we Can't make a local variable of a method as static it is class level variable it violates the concept of static
	}
	
	public void m2() {
		 int h;
		//System.out.println(h);
	}
//  public static void main(String[] args) {
//	name();
//	int k;
//	//System.out.println(k);
//	Static1 s=new Static1();
//	System.out.println(s.l);
//	
//}
	
}
