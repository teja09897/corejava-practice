package com.te.learn.strings;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ConvertArraytoString {
   public static void main(String[] args) {
	String[] s= {"java","is","programming","lang"};
	
    String string = Arrays.toString(s);
	System.out.println(string);
	
	StringBuilder sb=new StringBuilder();	
	for (int i = 0; i < s.length; i++) {
		sb= sb.append(s[i]);	
	}
	System.out.println(sb);
	
	String s1=String.join(" ",s);
	System.out.println(s1);
	
	List<String> collect = Arrays.stream(s).collect(Collectors.toList());
	System.out.println(collect.toString());
	
	
}
}
