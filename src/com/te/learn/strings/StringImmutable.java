package com.te.learn.strings;

import java.util.HashMap;

public class StringImmutable {
   public static void main(String[] args) {
	   //string constant pool area only for string objects because string is most 
	   //commonly used object in java thats why java people provided special memory management
	   //Inaddition of string all wrapper clases also immutable classes in java
	  String s="java";
	  s.concat("program");
	  System.out.println(s);
	  
	  
	  
	  StringBuffer  buffer=new StringBuffer("uytgf");
	  buffer.append("programmes");
	  System.out.println(buffer);
	  
	  StringBuilder  buffr=new StringBuilder("uytgf");
	  buffr.append("programmes");
	  System.out.println(buffr);
	  
	 // -128 to +127
	  int i1=100;
	  int i2=100;
	  System.out.println(i1==i2);
	  
	  HashMap< String, Integer>hashtable=new HashMap<String, Integer>();
	  hashtable.put(null, null);
	  System.out.println(hashtable);
	  
	  
}
}
