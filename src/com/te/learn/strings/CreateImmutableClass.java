package com.te.learn.strings;

import java.io.FileNotFoundException;
import java.io.PrintStream;

final  class Demo {
	private final int id;
	private final String name;

	public Demo(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;

	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return id + name;
	}

}
public class CreateImmutableClass {

	public static void main(String[] args) throws FileNotFoundException {
		Demo c = new Demo(12, "jhbv");
		Demo c1 = new Demo(13, "dfghjh");		
		System.out.println(c);
		PrintStream p=new PrintStream("knb");
		
		p.println("fdsa");
		
	}
}
