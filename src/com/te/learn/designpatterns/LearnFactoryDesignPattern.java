package com.te.learn.designpatterns;

import java.util.Scanner;

public class LearnFactoryDesignPattern {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("enter for os");
		String ostype = scanner.next();

		Os os = GetOsName.getOs(ostype);
		os.camera();
		os.performence();
	}
}
