package com.te.learn.designpatterns;

public class GetOsName {
	public static Os getOs(String osName) {
		if (osName.equalsIgnoreCase("Androidos")) {
			return new AndroidOs();
		}else if (osName.equalsIgnoreCase("miui")) {
			return new Miui();
		}
		return null;

	}
}
