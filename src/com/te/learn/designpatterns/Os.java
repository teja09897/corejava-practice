package com.te.learn.designpatterns;

public interface Os {
    public abstract void camera();
    public abstract void performence();
}
