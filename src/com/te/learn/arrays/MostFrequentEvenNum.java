package com.te.learn.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;

//Given an integer array nums, return the most frequent even element. If there is a tie, return the smallest one.
//If there is no such element return -1.
//
//input : nums = [0,1,2,2,4,4,1]
//o/p : 2
//Explanation: the even elements are 0 , 2, 4. of these 2 and 4 appears the most. We return the smallest element 2.
public class MostFrequentEvenNum {
	public static void main(String[] args) {
		int arr[] = { 0, 1, 2, 2, 4, 4, 1 };
		int fr[] = new int[arr.length];
		for (int i = 0; i < arr.length; i++) {
			int visited = -1;
			int count = 1;
			for (int j = i + 1; j < arr.length; j++) {
				if (arr[i] == arr[j]) {
					count++;
					fr[j] = visited;
				}
			}
			if (fr[i] != visited && count > 0 && arr[i] != 0 && arr[i] % 2 == 0) {				
				System.out.println(arr[i]+" fr "+count);

			}
		}
	}
}
