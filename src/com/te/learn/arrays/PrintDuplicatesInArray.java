package com.te.learn.arrays;

public class PrintDuplicatesInArray {
	public static void main(String[] args) {
		int[] arr = { 3, 5, 7, 8, 9, 12, 9, 12, 9, 12 };
		int fr[] = new int[arr.length];
		int visited = -1;
		for (int i = 0; i < arr.length; i++) {
			int count = 1;
			for (int j = i + 1; j < arr.length; j++) {
				if (arr[i] == arr[j]) {
					count++;
					fr[j] = visited;
				}
			}
			if (fr[i] != visited && count > 0) {
				System.out.println(arr[i]+" frequence "+count);
			}
		}

	}
}
