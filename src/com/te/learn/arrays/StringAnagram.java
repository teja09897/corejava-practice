package com.te.learn.arrays;

import java.util.Arrays;

public class StringAnagram {
	public static void main(String[] args) {
        String s1="SILENT";
        String s2="LISTEN";
        s1.toLowerCase();
        s2.toLowerCase();
        char[] cs = s1.toCharArray();
        char[] cs2 = s2.toCharArray();
        Arrays.sort(cs);
        Arrays.sort(cs2);
        if (Arrays.equals(cs, cs2)) {
			System.out.println("given string is anagram");
		}
        else {
			System.out.println("given string is not an anagram");
        }
	}
}
