package com.te.learn.arrays.methodoverloading;

class A {
	public static void m1(int i) {
      System.out.println(i);
	}
	 int m1(int i,int j) {
		return i+j;
	}
}

public class Demo {
	public static void main(String[] args) {
       A a=new A();
       a.m1(2);
       int m1 = a.m1(3, 4);
       System.out.println(m1);
	}
}
