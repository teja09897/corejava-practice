package com.te.learn.arrays;

public class PalindromeString {

	public static void main(String[] args) {
		String s = "madaM";
	String rev = "";

          for (int i = 0; i < s.length(); i++) {
			rev=s.charAt(i)+rev;
		}
          if (s.equalsIgnoreCase(rev)) {
			System.out.println("given String is palindrome");
		}
          else {
  			System.out.println("given String is not a palindrome");
          }
	}

}
