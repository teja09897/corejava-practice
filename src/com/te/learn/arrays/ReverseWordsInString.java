package com.te.learn.arrays;

public class ReverseWordsInString {
	public static void main(String[] args) {
		String s = "i love india";
		String[] words = s.split(" ");
		String rev="";
		for (int i = 0; i < words.length; i++) {
           rev=words[i]+" "+rev;
		}
		System.out.println(rev);
	}
}
