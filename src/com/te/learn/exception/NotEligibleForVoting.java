package com.te.learn.exception;

public class NotEligibleForVoting extends Exception {

	public NotEligibleForVoting(String message) {
		super(message);
	}
    
}
