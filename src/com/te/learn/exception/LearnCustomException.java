package com.te.learn.exception;

public class LearnCustomException {
	public static void main(String[] args) throws NotEligibleForVoting {
		int age = 19;
		if (age >= 18) {
			System.out.println("eligible for voting");
		} else {
			throw new NotEligibleForVoting("Not Eligible For Voting");
		}
	}
}
