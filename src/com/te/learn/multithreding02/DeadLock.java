package com.te.learn.multithreding02;
class A{
	public synchronized void d1(B b) {
		System.out.println("Thread1 starts execution of d1()");
		try{Thread.sleep(5000);}catch(Exception e) {}
		System.out.println("Thread1 trying to access B class last() ");
		b.last();
	}
	public synchronized void last() {
		System.out.println("Inside A last()");
	}
}
class B{
	public synchronized void d2(A a) {
		System.out.println("Thread2 starts execution of d2()");
		try{Thread.sleep(5000);}catch(Exception e) {}
		System.out.println("Thread2 trying to access A class last() ");
		a.last();
	}
	public synchronized void last() {
		System.out.println("Inside B last()");
	}
	
}
public class DeadLock extends Thread{
	A a=new A();
	B b=new B();
	public void m1() {
		this.start();//this line executed by main thread
		a.d1(b);
		
	}
	@Override
	public void run() {
		b.d2(a);//this line executed by child thread
		
	}
	
   public static void main(String[] args) {
	DeadLock lock=new DeadLock();
	lock.m1();
	
}
}
