package com.te.learn.multithreding02;

import java.util.Iterator;

class Display{
	public static synchronized void wish(String name) {
		for (int i = 0; i < 10; i++) {
			System.out.println("GM  ");
			//try {Thread.sleep(2000);}catch(Exception e) {};
			System.out.println(name);
		}
	}
	public void m1() {
		for (int i = 0; i < 5; i++) {
			System.out.println("non-synch");
		}
	}
}
class MyThread extends Thread{
	Display d;
	String name;
	public MyThread(Display d,String name) {
		this.d=d;
		this.name=name;
	}
	public void run() {
		d.wish(name);
		d.m1();
	}
}
public class Synchronized {
    public static void main(String[] args) {
    	Display d=new Display();
    	Display d2=new Display();
		MyThread t=new MyThread(d, "dhoni");
		MyThread t2=new MyThread(d2, "kolhi");
		
		t.start();
		t2.start();
	}
}
