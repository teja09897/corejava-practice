package com.te.learn.multithreding02;

class Display1{
	public synchronized void displayNum() {
		for (int i = 0; i <10; i++) {
			System.out.println(i);
		}
	}
	public synchronized void displayChar() {
		for (int i = 65; i < 75; i++) {
			System.out.println((char)i);
		}
	}
}
class MyThread1 extends Thread{
	Display1 d;
	public MyThread1(Display1 d) {
		this.d=d;
	}
	@Override
	public void run() {
		d.displayNum();
	}
}
class MyThread2 extends Thread{
	Display1 d;
	public MyThread2(Display1 d) {
		this.d=d;
	}
	@Override
	public void run() {
		d.displayChar();
	}
}
public class Synchrinized2 {
  public static void main(String[] args) {
	Display1 d=new Display1();
	MyThread1 t=new MyThread1(d);
	MyThread2 t2=new MyThread2(d);
	t.start();
	t2.start();
}
}
