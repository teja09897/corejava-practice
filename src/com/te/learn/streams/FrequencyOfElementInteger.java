package com.te.learn.streams;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class FrequencyOfElementInteger {
   public static void main(String[] args) {
	Integer a[]= {12,45,12,3,3,0,-1,1,-1};
	List<Integer> list = Arrays.asList(a);
	list.stream().collect(Collectors.groupingBy(e->e,Collectors.counting())).entrySet()
      .forEach(e->System.err.println(e.getKey() +" "+e.getValue()));
	
	
	list.stream().filter(e->Collections.frequency(list, e)>=1).collect(Collectors.groupingBy(e->e,Collectors.counting()))
	  .entrySet().forEach(e->System.out.println(e.getKey()+" "+e.getValue()));
	
	//Non-repeating element
	list.stream().filter(e->Collections.frequency(list, e)==1).limit(1).forEach(e->System.err.println(e));
	
	//=
	list.stream().filter(e->Collections.frequency(list, e)>1).limit(1).forEach(e->System.out.println(e));
   }
}
