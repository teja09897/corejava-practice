package com.te.learn.streams;

import java.util.Arrays;
import java.util.List;

public class DivisibleBy3 {
	public static void main(String[] args) {
//		List<Integer> asList = Arrays.asList(6, 9, 8, 4, 3);
//		asList.stream().filter(s -> s % 3 == 0).forEach(e -> System.out.println(e));
		int arr[]= {5,6,7,8};
		Arrays.sort(arr);
		for (int i : arr) {
			System.out.println(i);
		}
		System.out.println(arr.length-1);
		System.out.println(arr[arr.length-2]);
	}
}
