package com.te.learn.optionalclass;

import java.util.Optional;

public class LearnOptional {
public static void main(String[] args) {
	Employee employee=new Employee(12, 25,null, "teh@gmail.com");
	//System.out.println(employee.getEmpName().toLowerCase());
	
	Optional<String> ofNullable = Optional.ofNullable(employee.getEmpName());
	System.out.println(ofNullable.orElse("default"));
	
	Optional<Object> empty = Optional.empty();
	//System.out.println(empty);
	
	Optional<String> of = Optional.of(employee.getEmpName());
	//System.out.println(of);
}
}
