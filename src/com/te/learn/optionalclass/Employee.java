package com.te.learn.optionalclass;

public class Employee {
	private Integer empId;
	private Integer empAge;
	private String empName;
	private String empEmail;

	Employee() {
		super();
	}

	public Employee(Integer empId, Integer empAge, String empName, String empEmail) {
		super();
		this.empId = empId;
		this.empAge = empAge;
		this.empName = empName;
		this.empEmail = empEmail;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public Integer getEmpAge() {
		return empAge;
	}

	public void setEmpAge(Integer empAge) {
		this.empAge = empAge;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpEmail() {
		return empEmail;
	}

	public void setEmpEmail(String empEmail) {
		this.empEmail = empEmail;
	}

	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", empAge=" + empAge + ", empName=" + empName + ", empEmail=" + empEmail
				+ "]";
	}

}

