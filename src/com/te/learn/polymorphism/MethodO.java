package com.te.learn.polymorphism;

public class MethodO {
      public static int add(int i) {
		System.out.println(i);
		return i;
	}
       void add(int i,int j) {
		System.out.println(i+j);
	}
      public static void main(String[] args) {
		MethodO methodO = new MethodO();
		methodO.add(3,2);
	}
//	public static void main(String[] args) {
//		String main = MethodO.main("vbn");
//		System.out.println(main);
//	}
//
//	public static String main(String s) {
//		MethodO.main(6);
//		return s;
//	}
//
//	public static void main(int i) {
//		System.out.println(i);
//	}
}
