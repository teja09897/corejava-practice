package com.te.learn.threads;

class Object1 {
	synchronized void object1(Object2 obj2) {
		System.out.println("thread1");
		obj2.object2(this);
		System.out.println("thread1 end");

	}
}

class Object2 {
	synchronized void object2(Object1 obj1) {
		System.out.println("thread2");
		obj1.object1(this);
		System.out.println("thread2 end");

	}

}

public class LearnDeadlock {
	public static void main(String[] args) {

		Object1 obj1 = new Object1();
		Object2 obj2 = new Object2();

		Runnable run1 = new Runnable() {

			@Override
			public void run() {

				obj1.object1(obj2);
			}
		};
		Runnable run2 = new Runnable() {

			@Override
			public void run() {

				obj2.object2(obj1);
			}
		};
		Thread t1=new Thread(run1);
		Thread t2=new Thread(run2);
		t1.start();
		t2.start();
		t2.getState();
	}

}
