package com.te.logicalprogrammes;
class A{
	
}
public class Demo extends A{
	protected Demo(){
		super();
	}
	protected void msg() {
		System.out.println("protected method");
	}
	
	public static void main(String[] args) {
		Demo d=new Demo();
		d.msg();
		
	}
}
