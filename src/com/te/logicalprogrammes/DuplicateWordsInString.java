package com.te.logicalprogrammes;

import java.util.Iterator;

public class DuplicateWordsInString {
   public static void main(String[] args) {
	String s="java is an object java is pi java is secure language 0vhvb 0vhvb";
	String[] split = s.split(" ");
	for (int i = 0; i < split.length; i++) {
		int count=1;
		for (int j = i+1; j < split.length; j++) {
			if (split[i].equals(split[j])) {
				split[j]="0";
				count++;
			}
		}
		if (split[i]!="0"&&count>1) {
			System.out.println(split[i]);
		}
	}
	
}
}
