package com.te.logicalprogrammes;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class StreamMax {
   public static void main(String[] args) {
	List<Integer> asList = Arrays.asList(4,5,6,7,88);
	Optional<Integer> max = asList.stream().max(( o1, o2)->o1.compareTo(o2));
	System.out.println(max);
	
	int []i= {1,2,3,4};
	Stream<Integer> boxed = Arrays.stream(i).boxed();
	boxed.forEach(e->System.out.println(e));
	
	
	
	
}
}
