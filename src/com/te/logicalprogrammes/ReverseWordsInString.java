package com.te.logicalprogrammes;

public class ReverseWordsInString {
	public static void main(String[] args) {
       String s="i love india";
       String rev="";
       String[] strings = s.split(" ");
       for (int i = 0; i < strings.length; i++) {
		rev=strings[i]+" "+rev;
	}
       System.out.println(rev);
       
	}
}
