package com.te.logicalprogrammes;

import java.util.Arrays;
import java.util.Iterator;

public class CompareTwoArrays {
	public static void main(String[] args) {
		int arr1[] = { 1, 3, 4, 5 };
		int arr2[] = { 3, 1, 4, 6 };

		
		if (compareArrays(arr1, arr2)) {
			System.out.println("given arrays are equal");
		} else {
			System.out.println("given arrays are not equal");
		}
	}

	public static boolean compareArrays(int arr1[], int arr2[]) {
		int n = arr1.length;
		int n1 = arr2.length;
		if (n != n1) {
			return false;
		} else {
			Arrays.sort(arr1);
			Arrays.sort(arr2);

			for (int i = 0; i < n; i++) {
				if (arr1[i] != arr2[i]) {
					return false;
				} else {
					return true;
				}
			}
			return true;
		}

	}
}
