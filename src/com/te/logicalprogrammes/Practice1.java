package com.te.logicalprogrammes;

import java.util.Arrays;
import java.util.List;

public class Practice1 {
   public static void main(String[] args) {
	List<Integer> asList = Arrays.asList(4,5,6,7,3,2,1);
	asList.stream().map(e->e*2).sorted().forEach(e->System.out.println(e));
}
}
