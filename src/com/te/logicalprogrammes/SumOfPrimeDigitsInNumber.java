package com.te.logicalprogrammes;

public class SumOfPrimeDigitsInNumber {
   public static void main(String[] args) {
	int num=12345;
	int count=0;
	while (num>0) {
		num=num%10;
		for (int i = 2; i < num; i++) {
			if (num%2==1) {
				count++;
			}
		}
		num=num/10;
	}
	
}
}
