package com.te.java8features;
interface A{
	
	public static final String NAME = "vbnbnb";

	void m();
	default void m1() {
		System.out.println(NAME);
		System.out.println("default method");
	}
	static void m2() {
		System.out.println("static method");
	}
	
}
public class DefaultMethods implements A{
int i=10;
	@Override
	public void m() {
		System.out.println(this.i);
		System.out.println("impl");
		
	}
	
	
//	public void m1() {
//		System.out.println("default method---");
//	}
	
	public static void main(String[] args) {
		DefaultMethods df=new DefaultMethods();
		df.m();
		df.m1();
		A.m2();
	}

}
