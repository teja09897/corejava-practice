package com.te.designpatterns;

public class LearnBuilderDesign {
    public static void main(String[] args) {
		Employee build = new EmployeeBuilder().empAge(123).empEmail("kjnhbv").empId(56).build();
		System.out.println(build);
	}
}
