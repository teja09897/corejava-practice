package com.te.designpatterns;

public class EmployeeBuilder {
	private Integer empId;
	private Integer empAge;
	private String empName;
	private String empEmail;

	public EmployeeBuilder() {
		super();
	}
	public EmployeeBuilder(Integer empId, Integer empAge, String empName, String empEmail) {
		super();
		this.empId = empId;
		this.empAge = empAge;
		this.empName = empName;
		this.empEmail = empEmail;
	}

	public EmployeeBuilder empId(Integer empId) {
		this.empId = empId;
		return this;
	}

	public EmployeeBuilder empAge(Integer empAge) {
		this.empAge = empAge;
		return this;
	}

	public EmployeeBuilder empName(String empName) {
		this.empName = empName;
		return  this;
	}

	public EmployeeBuilder empEmail(String empEmail) {
		this.empEmail = empEmail;
		return this;
	}
	
	public Employee build() {
		
		return new Employee(this.empId, this.empAge, this.empName, this.empEmail);
		
	}
	
}
