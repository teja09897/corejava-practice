package com.te.designpatterns;

public class LearnSingleton {
	static LearnSingleton learnSingleton;

	private LearnSingleton() {

	}

	public static LearnSingleton getObj() {
		if (learnSingleton == null) {
			learnSingleton = new LearnSingleton();
			return learnSingleton;
		} else {
			return learnSingleton;
		}
	}

	public static void main(String[] args) {
		LearnSingleton learnSingleton = getObj();
		LearnSingleton learnSingleton2 = getObj();

		System.out.println(learnSingleton.equals(learnSingleton2));

	}
}
