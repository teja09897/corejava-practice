package com.te.learnobjectclass;

public class DuplicatesCharInString {
   public static void main(String[] args) {
	String s="raviteja vas";
	char[] charArray = s.toCharArray();
	char arr[]=new char[s.length()];
	for (int i = 0; i < charArray.length; i++) {
		int count=1;
		char visited='\u0000';
		for (int j = i+1; j < charArray.length; j++) {
			if (charArray[i]==charArray[j]) {
				count++;
				charArray[j]=visited;
			}
		}
		if (count>1) {
			System.out.println(charArray[i]);
		}
	}
}
}
