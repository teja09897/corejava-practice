package com.te.learnobjectclass;

public class Test {
	public static void main(String[] args) {
		Student student01=new Student(12, "name01");
	    int stu01 = student01.hashCode();
	    System.out.println(stu01);
	    
	    Student student02=new Student(13, "name02");
	    int stu02 = student02.hashCode();
	    System.out.println(stu02);
		
		String s=new String("ravi");
		int ravi = s.hashCode();
		System.out.println(ravi);
		
		String s1=new String("teja");
		int tej = s1.hashCode();
		System.out.println(tej);
		
		String d="FB";
		String g="Ea";
		System.out.println(d.equals(g));
		System.out.println(d.hashCode()+"   "+g.hashCode());
		
		
	
	}

}
