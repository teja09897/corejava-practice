package com.te.learnobjectclass;

public class Test1 {
	 public static void main(String[] args) {
		Student s1=new Student(12, "lkjh");
		Student s2=new Student(67, "mnbvcx");
		System.out.println(s1==s2);
		Student s3=s2;
		System.out.println(s2==s2);
		System.out.println(s1.equals(s2));
		System.out.println(s2.equals(s3));
		//======================================
		
		String str1=new String("hgfds");
		//String str2=new String("hgfds");
		String str2="hgfds";
		System.out.println(str1==str2);
		System.out.println(str1.equals(str2));
		String str3=str2;
		System.out.println(str2.equals(str3));
	}

}
