package com.te.learnobjectclass;

public class DuplicatesInArray {
  public static void main(String[] args) {
	int arr[]= {5,6,7,8,6,9,9,0};	
	int fr[]=new int[arr.length];
	for (int i = 0; i < arr.length; i++) {
		 int count=1;
		 int visited=-1;
		for (int j = i+1; j < arr.length; j++) {
			if (arr[i]==arr[j]) {
				count++;
				fr[j]=visited;
			}
		}
		if (count>0&&fr[i]!=visited ){
			System.out.println(arr[i]+"fr "+count );
		}
	}
}
}
