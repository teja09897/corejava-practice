package com.te.methodoverloading;
class Animal{
	  String m1() {
		
		return "animal";
	}
}
class Dog extends Animal{
	
	 public String m1() {
		System.out.println("dog....");
		return "dog";
	}
	
}
public class LearnMethodOverriding {
     public static void main(String[] args) {
		Animal animal=new Dog();
		//Dog dog=(Dog)animal;
		animal.m1();
    	 
	}
}
