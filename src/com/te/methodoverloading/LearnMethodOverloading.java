package com.te.methodoverloading;
class Aa{
	public int m1(int i) {
		return i;
		
	}
	public static void m1() {
		
	}
	public int m1(int i,int j) {
		return i+j;
		
	}
	
}
public class LearnMethodOverloading {
	public static void main(String[] args) {
		Aa a=new Aa();
		a.m1();
		int m1 = a.m1(12);
		System.out.println(m1);
		int m12 = a.m1(45, 56);
		System.out.println(m12);
	}

}
