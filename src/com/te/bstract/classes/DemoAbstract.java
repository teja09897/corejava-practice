package com.te.bstract.classes;

import javax.naming.event.ObjectChangeListener;

abstract class Demo {
	public int i;

	  abstract void m1()throws RuntimeException;

	public Demo(int i) {
		this.i=i;
	}

	public static void m2() {
		System.out.println("concreate static method");
	}

	public void m3() {
		System.out.println("concreate non-static method");
	}

	{
		System.out.println("non static initializeer");
	}

	static {
		System.out.println(" static initializeer");
	}
}

public class DemoAbstract extends Demo {
	public DemoAbstract() {
		super(4);
	}

	@Override
	public void m1() {
		System.out.println("impl");

	}

	public static void main(String[] args) throws ArithmeticException {
		
		DemoAbstract d = new DemoAbstract();
		
		Demo demo;
		demo=d;
		demo.m1();
		m2();
		demo.m3();
		System.out.println(demo.i);

	}
}


