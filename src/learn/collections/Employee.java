package learn.collections;

public class Employee implements Comparable<Employee>{
	private int empId;
	private String empName;
	private int empAge;
	private String empEmail;
	public Employee() {
		super();
	}
	public Employee(int empId, String empName, int empAge, String empEmail) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.empAge = empAge;
		this.empEmail = empEmail;
	}
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public int getEmpAge() {
		return empAge;
	}
	public void setEmpAge(int empAge) {
		this.empAge = empAge;
	}
	public String getEmpEmail() {
		return empEmail;
	}
	public void setEmpEmail(String empEmail) {
		this.empEmail = empEmail;
	}
	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", empName=" + empName + ", empAge=" + empAge + ", empEmail=" + empEmail
				+ "]";
	}

	@Override
	public int compareTo(Employee o) {
		return this.getEmpAge()-o.getEmpAge();
	}
	
	
	

}
