package learn.collections;

import java.util.Comparator;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.stream.Stream;

public class LearnHashset {
	public static void main(String[] args) {
		HashSet<Employee> hashSet=new HashSet<Employee>();
		hashSet.add(new Employee(12, "kjhgf", 23, "kjhgf@gmail.com"));
		hashSet.add(new Employee(10, "viany", 20, "viany@gmail.com"));
		hashSet.add(new Employee(13, "kumar", 22, "kumar@gmail.com"));
		hashSet.add(new Employee(11, "srikar", 24, "srikar@gmail.com"));
		hashSet.add(new Employee(11, "srikar", 24, "srikar@gmail.com"));
		
		//hashSet.stream().sorted((o1,o2)->(o1.getEmpAge()-o2.getEmpAge())).forEach(e->System.out.println(e));
		TreeSet<Employee> treeSet=new TreeSet<Employee>(hashSet);
		//Comparator<Employee>  comparator=(o1,o2)->o1.getEmpAge()-o2.getEmpAge();
		treeSet.stream().forEach(e->System.out.println(e));
		
		
		
		
		
	}

}
