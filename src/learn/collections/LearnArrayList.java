package learn.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class LearnArrayList {
      public static void main(String[] args) {
		List<Employee> employees=new ArrayList<Employee>();
		employees.add(new Employee(12, "kjhgf", 23, "kjhgf@gmail.com"));
		employees.add(new Employee(10, "viany", 20, "viany@gmail.com"));
		employees.add(new Employee(13, "kumar", 22, "kumar@gmail.com"));
		employees.add(new Employee(11, "srikar", 24, "srikar@gmail.com"));
		
//		for (int i = 0; i <employees.size(); i++) {
//			System.out.println(employees.get(i));
//		}
//			
//		for (Employee employee : employees) {
//			System.out.println(employee);
//		}
		
		employees.stream().forEach(e->System.out.println(e));
        Comparator<Employee> c= (o1,o2)->o1.getEmpAge()-o2.getEmpAge();
		Collections.sort(employees,c);
		employees.stream().forEach(e->System.out.println(e));
		
		
		
		
		
		
	}
}
