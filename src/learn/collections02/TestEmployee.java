package learn.collections02;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestEmployee {
	public static void main(String[] args) {
		List<Employee> employeeList = new ArrayList<Employee>();

		employeeList.add(new Employee(111, "Jiya Brein", 32, "Female", "HR", 2011, 25000.0));
		employeeList.add(new Employee(122, "Paul Niksui", 25, "Male", "Sales And Marketing", 2015, 13500.0));
		employeeList.add(new Employee(133, "Martin Theron", 29, "Male", "Infrastructure", 2012, 18000.0));
		employeeList.add(new Employee(144, "Murali Gowda", 28, "Male", "Product Development", 2014, 32500.0));
		employeeList.add(new Employee(155, "Nima Roy", 27, "Female", "HR", 2013, 22700.0));
		employeeList.add(new Employee(166, "Iqbal Hussain", 43, "Male", "Security And Transport", 2016, 10500.0));
		employeeList.add(new Employee(177, "Manu Sharma", 35, "Male", "Account And Finance", 2010, 27000.0));
		employeeList.add(new Employee(188, "Wang Liu", 31, "Male", "Product Development", 2015, 34500.0));
		employeeList.add(new Employee(199, "Amelia Zoe", 24, "Female", "Sales And Marketing", 2016, 11500.0));
		employeeList.add(new Employee(200, "Jaden Dough", 38, "Male", "Security And Transport", 2015, 11000.5));
		employeeList.add(new Employee(211, "Jasna Kaur", 27, "Female", "Infrastructure", 2014, 15700.0));
		employeeList.add(new Employee(222, "Nitin Joshi", 25, "Male", "Product Development", 2016, 28200.0));
		employeeList.add(new Employee(233, "Jyothi Reddy", 27, "Female", "Account And Finance", 2013, 21300.0));
		employeeList.add(new Employee(244, "Nicolus Den", 24, "Male", "Sales And Marketing", 2017, 10700.5));
		employeeList.add(new Employee(255, "Ali Baig", 23, "Male", "Infrastructure", 2018, 12700.0));
		employeeList.add(new Employee(266, "Sanvi Pandey", 26, "Female", "Product Development", 2015, 28900.0));
		employeeList.add(new Employee(277, "Anuj Chettiar", 31, "Male", "Product Development", 2012, 35700.0));

		long countOfMaleEmp = employeeList.stream().filter(e -> e.gender.equals("Male")).count();
		System.out.println(countOfMaleEmp);

		long countOfFemaleEmp = employeeList.stream().filter(e -> e.gender.equals("Female")).count();
		System.out.println(countOfFemaleEmp);

		// departmrnts in organization
		employeeList.stream().collect(Collectors.groupingBy(Employee::getDepartment)).entrySet()
				.forEach(e -> System.out.println(e.getKey()));

		Double avgAgeOfMale = employeeList.stream().filter(e -> e.gender.equals("Male"))
				.collect(Collectors.averagingInt(Employee::getAge));
		System.out.println(avgAgeOfMale);

		Predicate<Employee> p = e -> e.getAge() > 25;
		employeeList.stream().collect(Collectors.partitioningBy(p)).entrySet()
				.forEach(e -> System.out.println(e.getKey() + " " + e.getValue()));

		//highest paying employee in the organisaton
		Optional<Employee> collect = employeeList.stream().collect(Collectors.maxBy(Comparator.comparingDouble(Employee::getSalary)));
		System.err.println(collect.get());

		//count the no of employees in each dept
		employeeList.stream().collect(Collectors.groupingBy(Employee::getDepartment, Collectors.counting())).entrySet()
				.forEach(e -> System.out.println(e.getKey() + " " + e.getValue()));
		
		//names of all employees who was joined after 2015
		employeeList.stream().filter(e->e.yearOfJoining>2015).forEach(e->System.err.println(e.name));
		
		//Avg Sal of each dept
		employeeList.stream().collect(Collectors.groupingBy(Employee::getDepartment,Collectors.averagingDouble(Employee::getSalary))).entrySet().forEach(e->System.out.println(e.getKey()+"=    "+e.getValue()));
		
		//youngestMaleEmp in Product Development dept
		Optional<Employee> youngestMaleEmp = employeeList.stream().filter(e->e.department.equals("Product Development")).filter(e->e.gender.equals("Male")).sorted((e1,e2)->e1.age-e2.getAge()).findFirst();
	    System.out.println(youngestMaleEmp.get());
	    
	    //mostExpEmp in org
	    Optional<Employee> mostExpEmp = employeeList.stream().sorted((e1,e2)->e2.getYearOfJoining()-e1.getYearOfJoining()).findFirst();
	    System.err.println(mostExpEmp.get());
	    
	    //MaleEmpInSalesDept
	    long MaleEmpInSalesDept = employeeList.stream().filter(e->e.getDepartment().equals("Sales And Marketing")).filter(e->e.getGender().equals("Male")).count();
	    System.out.println(MaleEmpInSalesDept);
	    
	    Double avgSalOfMale = employeeList.stream().filter(e->e.getGender().equals("Male")).collect(Collectors.averagingDouble(Employee::getSalary));
	    System.out.println(avgSalOfMale);
	    
	    employeeList.stream().collect(Collectors.groupingBy(Employee::getDepartment)).entrySet().forEach(e->System.out.println(e));
	    
	    DoubleSummaryStatistics totalSal = employeeList.stream().collect(Collectors.summarizingDouble(Employee::getSalary));
	    System.out.println(totalSal);
	    
	    Predicate<Employee> predicate=r->r.age>=25;
	    employeeList.stream().collect(Collectors.partitioningBy(predicate)).entrySet().forEach(e->System.out.println(e));;
	    

	 
	    
	    
	   
	}
}
